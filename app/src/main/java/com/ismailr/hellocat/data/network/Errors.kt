package com.ismailr.hellocat.data.network

/**
 * Created by IsmailR on 1/22/21.
 */
sealed class Errors : Throwable() {
    data class NetworkError(val code: Int = -1, val desc: String = "") : Errors()
    object EmptyResultsError : Errors()
}