package com.ismailr.hellocat.data.model

/**
 * Created by IsmailR on 1/21/21.
 */
data class Image(
    val url: String?
)