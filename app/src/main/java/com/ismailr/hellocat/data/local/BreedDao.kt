package com.ismailr.hellocat.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

/**
 * Created by IsmailR on 1/22/21.
 */
@Dao
interface BreedDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertData(data: List<BreedEntity>)

    @Query("SELECT * from breed")
    suspend fun queryData(): List<BreedEntity>
}