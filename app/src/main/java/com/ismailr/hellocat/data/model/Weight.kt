package com.ismailr.hellocat.data.model

/**
 * Created by IsmailR on 1/21/21.
 */
data class Weight(
    val metric: String?
)