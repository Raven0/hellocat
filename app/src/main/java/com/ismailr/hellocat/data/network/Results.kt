package com.ismailr.hellocat.data.network

/**
 * Created by IsmailR on 1/22/21.
 */
sealed class Results<out T> {
    companion object {
        fun <T> success(result: T): Results<T> = Success(result)
        fun <T> failure(error: Throwable): Results<T> = Failure(error)
    }

    data class Failure(val error: Throwable) : Results<Nothing>()
    data class Success<out T>(val data: T) : Results<T>()
}