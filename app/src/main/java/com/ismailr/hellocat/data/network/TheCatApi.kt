package com.ismailr.hellocat.data.network

import com.ismailr.hellocat.data.model.Breed
import retrofit2.Response
import retrofit2.http.GET

/**
 * Created by IsmailR on 1/21/21.
 */
interface TheCatApi {
    @GET("breeds")
    suspend fun getBreeds(): Response<List<Breed>>
}