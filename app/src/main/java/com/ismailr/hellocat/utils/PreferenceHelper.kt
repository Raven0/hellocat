package com.ismailr.hellocat.utils

import android.content.Context
import android.content.SharedPreferences

/**
 * Created by IsmailR on 1/22/21.
 */
@Suppress("unused")
class PreferenceHelper(private val context: Context){
    fun prefs(name: String): SharedPreferences = context.getSharedPreferences(name, Context.MODE_PRIVATE)
}