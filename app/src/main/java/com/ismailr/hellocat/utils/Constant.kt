package com.ismailr.hellocat.utils

/**
 * Created by IsmailR on 1/21/21.
 */
const val BASE_URL = "https://api.thecatapi.com/v1/"
const val DB_NAME = "app.db"