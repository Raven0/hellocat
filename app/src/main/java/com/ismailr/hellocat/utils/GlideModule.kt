package com.ismailr.hellocat.utils

import android.content.Context
import com.bumptech.glide.Glide
import com.bumptech.glide.GlideBuilder
import com.bumptech.glide.Registry
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.load.engine.cache.InternalCacheDiskCacheFactory
import com.bumptech.glide.load.engine.cache.LruResourceCache
import com.bumptech.glide.module.AppGlideModule

/**
 * Created by IsmailR on 1/22/21.
 */
@Suppress("unused")
@GlideModule
class GlideModule : AppGlideModule() {
    override fun registerComponents(context: Context, glide: Glide, registry: Registry) {

    }

    override fun applyOptions(context: Context, builder: GlideBuilder) {
        builder.setDiskCache(InternalCacheDiskCacheFactory(context,
                diskCacheFolderName(),
                diskCacheSizeBytes()))
                .setMemoryCache(LruResourceCache(memoryCacheSizeBytes().toLong()))
    }

    override fun isManifestParsingEnabled(): Boolean {
        return false
    }

    private fun memoryCacheSizeBytes(): Int {
        return 1024 * 1024 * 20 // 20 MB
    }

    private fun diskCacheSizeBytes(): Long {
        return 1024 * 1024 * 512 // 512 MB
    }

    private fun diskCacheFolderName(): String {
        return "mvvm-app"
    }
}