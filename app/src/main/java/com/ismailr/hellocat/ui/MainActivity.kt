package com.ismailr.hellocat.ui
//21 January 2021 22.30
import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.ismailr.hellocat.R
import com.ismailr.hellocat.data.local.toDataList
import com.ismailr.hellocat.data.model.Breed
import com.ismailr.hellocat.databinding.ActivityMainBinding
import com.ismailr.hellocat.utils.*
import com.ismailr.hellocat.viewmodel.BreedViewModel
import com.ismailr.hellocat.viewstate.BreedViewState
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var adapter: DropDownAdapter
    private lateinit var failedImage: String
    private val viewModel: BreedViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        bind()
    }

    private fun bind(){
        adapter = DropDownAdapter(this)
        binding.spinnerBreeds.adapter = adapter
        binding.spinnerBreeds.onItemSelectedListener = object: AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if(position != 0){
                    bindBreed(adapter.getItem(position))
                    binding.ivGif.makeGone()
                    binding.cardCat.makeVisible()
                }else{
                    binding.ivGif.makeVisible()
                    binding.cardCat.makeGone()
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                binding.ivGif.makeVisible()
            }
        }

        binding.btnRefresh.setOnClickListener {
            loadImage(failedImage)
            binding.btnRefresh.makeGone()
        }

        Glide.with(this).load(R.drawable.cat_waiting).into(binding.ivGif)

        viewModel.getBreeds()
        observe(viewModel.stateLiveData, this::onNewState)
    }

    @SuppressLint("SetTextI18n")
    private fun bindBreed(breed: Breed){
        if (!breed.image?.url.isNullOrEmpty()){
            breed.image?.url?.let { loadImage(it) }
        }else{
            toast { "Image Unavailable" }
            Glide.with(this).load(R.drawable.cat_cute).into(binding.ivCatImage)
        }

        binding.tvCatName.text = "${breed.name} - ${breed.id}"
        binding.tvCatDesc.text = breed.description
        binding.tvCatCountry.text = breed.country_code?.let { loadCountry(it) + loadCountry(it) + loadCountry(it) }

        binding.btnWiki.setOnClickListener {
            startActivity(Intent(Intent.ACTION_VIEW).setData(Uri.parse(breed.wikipedia_url)))
        }
    }

    private fun loadCountry(country: String): String {
        val flagOffset = 0x1F1E6
        val asciiOffset = 0x41

        val firstChar = Character.codePointAt(country, 0) - asciiOffset + flagOffset
        val secondChar = Character.codePointAt(country, 1) - asciiOffset + flagOffset

        return (String(Character.toChars(firstChar)) + String(Character.toChars(secondChar)))
    }

    private fun loadImage(url: String){
        val circularProgressDrawable = CircularProgressDrawable(this)
        circularProgressDrawable.strokeWidth = 5f
        circularProgressDrawable.centerRadius = 30f
        circularProgressDrawable.start()

        Glide.with(this)
                .load(url)
                .placeholder(circularProgressDrawable)
                .addListener(object : RequestListener<Drawable> {
                    @SuppressLint("SetTextI18n")
                    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                        toast { "Unable to load (${e?.message})" }
                        circularProgressDrawable.stop()
                        failedImage = url
                        binding.btnRefresh.makeVisible()
                        return true
                    }

                    override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                        circularProgressDrawable.stop()
                        binding.btnRefresh.makeGone()
                        return false
                    }
                })
                .into(binding.ivCatImage)
    }

    private fun onNewState(state: BreedViewState) {
        log { state.toString() }

        if (state.throwable != null) {
            when (state.throwable) {
                else -> state.throwable.toString()
            }.also { str ->
                toast { str }
            }
        }

        state.data?.let {
            adapter.addAll(it.toList().toDataList())
        }
    }
}