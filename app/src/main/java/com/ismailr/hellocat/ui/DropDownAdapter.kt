package com.ismailr.hellocat.ui

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.ismailr.hellocat.R
import com.ismailr.hellocat.data.local.breedDummy
import com.ismailr.hellocat.data.model.Breed
import java.util.ArrayList

/**
 * Created by IsmailR on 1/22/21.
 */
class DropDownAdapter(context: Context) : BaseAdapter() {
    private var data: MutableList<Breed> = ArrayList()
    private val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    fun addAll(list: List<Breed>){
        data.clear()
        data.add(0, breedDummy())
        for (i in list){
            data.add(i)
        }
        notifyDataSetChanged()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View
        val vh: ItemHolder
        if (convertView == null) {
            view = inflater.inflate(R.layout.spinner_item, parent, false)
            vh = ItemHolder(view)
            view?.tag = vh
        } else {
            view = convertView
            vh = view.tag as ItemHolder
        }
        vh.label.text = data[position].name

        return view
    }

    override fun getItem(position: Int): Breed {
        return data[position]
    }

    override fun getCount(): Int {
        return data.size
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    private class ItemHolder(row: View?) {
        val label: TextView = row?.findViewById(R.id.tvBreed) as TextView
    }

}