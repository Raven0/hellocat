package com.ismailr.hellocat.ui

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.ismailr.hellocat.R
import com.ismailr.hellocat.databinding.ActivitySplashScreenBinding
import com.ismailr.hellocat.utils.*
import com.ismailr.hellocat.viewmodel.BreedViewModel
import com.ismailr.hellocat.viewstate.BreedViewState
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SplashScreen : AppCompatActivity() {
    private lateinit var binding: ActivitySplashScreenBinding
    private val viewModel: BreedViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySplashScreenBinding.inflate(layoutInflater)
        setContentView(binding.root)

        bind()
    }

    private fun bind(){
        Glide.with(this).load(R.drawable.cat_cute).into(binding.ivGif)

        viewModel.getBreeds()
        observe(viewModel.stateLiveData, this::onNewState)
    }

    private fun onNewState(state: BreedViewState) {
        log { state.toString() }

        if (state.throwable != null) {
            when (state.throwable) {
                else -> state.throwable.toString()
            }.also { str ->
                toast { str }
            }
        }

        state.data?.let {
            Handler(Looper.getMainLooper()).postDelayed({
                startActivity(Intent(this, MainActivity::class.java).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
            }, 1500)
        }

        if (state.isLoading) binding.mProgressBar.makeVisible() else binding.mProgressBar.makeInvisible()
    }
}