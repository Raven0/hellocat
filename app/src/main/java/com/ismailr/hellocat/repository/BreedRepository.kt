package com.ismailr.hellocat.repository

import androidx.annotation.AnyThread
import com.ismailr.hellocat.base.repository.BaseRepositoryBoth
import com.ismailr.hellocat.base.repository.ILocalDataSource
import com.ismailr.hellocat.base.repository.IRemoteDataSource
import com.ismailr.hellocat.data.local.AppDatabase
import com.ismailr.hellocat.data.local.BreedEntity
import com.ismailr.hellocat.data.local.toDataEntityList
import com.ismailr.hellocat.data.model.Breed
import com.ismailr.hellocat.data.network.Results
import com.ismailr.hellocat.data.network.TheCatApi
import com.ismailr.hellocat.utils.processApiResponse
import javax.inject.Inject

/**
 * Created by IsmailR on 1/21/21.
 */
class BreedRepository @Inject constructor(
    remoteDataSource: RemoteDataSource,
    localDataSource: LocalDataSource
) : BaseRepositoryBoth<RemoteDataSource, LocalDataSource>(remoteDataSource, localDataSource) {
    suspend fun fetchData(): Results<List<BreedEntity>> {
        val data = localDataSource.fetchBreedSource()
        return if (data.isEmpty()){
            return when (val remote = remoteDataSource.getBreed()) {
                is Results.Success -> {
                    val result = remote.data.toList().toDataEntityList()
                    localDataSource.insertDataInternal(result)
                    Results.success(result)
                }
                is Results.Failure -> {
                    Results.failure(remote.error)
                }
            }
        }else{
            Results.success(data)
        }
    }
}

class RemoteDataSource @Inject constructor(
    private var apiService: TheCatApi
) : IRemoteDataSource {
    suspend fun getBreed(): Results<List<Breed>> {
        val data = processApiResponse {
            apiService.getBreeds()
        }
        return when (data) {
            is Results.Success -> {
                data
            }
            is Results.Failure -> {
                Results.failure(data.error)
            }
        }
    }
}

class LocalDataSource @Inject constructor(
    private var db: AppDatabase,
) : ILocalDataSource {
    @AnyThread
    suspend fun fetchBreedSource(): List<BreedEntity> {
        return db.breedDao().queryData()
    }

    @AnyThread
    suspend fun insertDataInternal(data: List<BreedEntity>) {
        db.breedDao().insertData(data)
    }
}