package com.ismailr.hellocat.di

import android.app.Application
import androidx.room.Room
import com.ismailr.hellocat.data.local.AppDatabase
import com.ismailr.hellocat.data.network.ApiService
import com.ismailr.hellocat.data.network.TheCatApi
import com.ismailr.hellocat.data.model.Breed
import com.ismailr.hellocat.repository.BreedRepository
import com.ismailr.hellocat.repository.LocalDataSource
import com.ismailr.hellocat.repository.RemoteDataSource
import com.ismailr.hellocat.utils.DB_NAME
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Singleton

/**
 * Created by IsmailR on 1/21/21.
 */
@Module
@InstallIn(ApplicationComponent::class)
object AppModule {
    @Singleton
    @Provides
    fun provideApi(): TheCatApi = ApiService.getClient()

    @Singleton
    @Provides
    fun provideAppDatabase(application: Application): AppDatabase {
        return Room.databaseBuilder(
            application,
            AppDatabase::class.java,
            DB_NAME
        ).fallbackToDestructiveMigration().build()
    }

    @Provides
    fun provideBreedRepository(
        remoteDataSource: RemoteDataSource,
        localDataSource: LocalDataSource
    ) = BreedRepository(remoteDataSource, localDataSource)

    @Provides
    fun provideListData() = ArrayList<Breed>()
}