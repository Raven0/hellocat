package com.ismailr.hellocat.base.repository

/**
 * Created by IsmailR on 1/22/21.
 */
interface IRepository

interface IRemoteDataSource

interface ILocalDataSource