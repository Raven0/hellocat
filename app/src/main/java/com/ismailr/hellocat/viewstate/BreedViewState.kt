package com.ismailr.hellocat.viewstate

import com.ismailr.hellocat.data.local.BreedEntity

/**
 * Created by IsmailR on 1/22/21.
 */
data class BreedViewState(
    val isLoading: Boolean,
    val throwable: Throwable?,
    val data: List<BreedEntity>?,
    val isSuccesfull: Boolean
) {

    companion object {
        fun initial(): BreedViewState {
            return BreedViewState(
                isLoading = false,
                throwable = null,
                data = null,
                isSuccesfull = false
            )
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as BreedViewState

        if (isLoading != other.isLoading) return false
        if (throwable != other.throwable) return false
        if (data != other.data) return false
        if (isSuccesfull != other.isSuccesfull) return false

        return true
    }

    override fun hashCode(): Int {
        var result = isLoading.hashCode()
        result = 31 * result + (throwable?.hashCode() ?: 0)
        result = 31 * result + (data?.hashCode() ?: 0)
        result = 31 * result + isSuccesfull.hashCode()
        return result
    }
}