package com.ismailr.hellocat.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ismailr.hellocat.data.network.Results
import com.ismailr.hellocat.repository.BreedRepository
import com.ismailr.hellocat.utils.postNext
import com.ismailr.hellocat.viewstate.BreedViewState
import kotlinx.coroutines.launch

/**
 * Created by IsmailR on 1/22/21.
 */
class BreedViewModel @ViewModelInject constructor(
    private val repository: BreedRepository
) : ViewModel() {
    private val _stateLiveData: MutableLiveData<BreedViewState> = MutableLiveData(BreedViewState.initial())
    val stateLiveData: LiveData<BreedViewState> = _stateLiveData

    init {
        viewModelScope.launch {
            _stateLiveData.postNext { state ->
                state.copy(
                    isLoading = false,
                    throwable = null,
                    data = null,
                    isSuccesfull = false
                )
            }
        }
    }

    fun getBreeds() {
        viewModelScope.launch {
            _stateLiveData.postNext {
                it.copy(isLoading = true, throwable = null, data = null, isSuccesfull = false)
            }
            when (val result = repository.fetchData()) {
                is Results.Failure -> _stateLiveData.postNext {
                    it.copy(isLoading = false, throwable = result.error, data = null, isSuccesfull = false)
                }
                is Results.Success -> _stateLiveData.postNext {
                    it.copy(isLoading = false, throwable = null, data = result.data, isSuccesfull = true)
                }
            }
        }
    }
}